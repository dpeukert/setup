#!/bin/bash

log 'COPYING KAHLO CONFIG'
	mkdir --parents "/home/$device/.config/kahlo/"
	cat "/home/$device/private/isidore_files/$device/kahlo-config" > "/home/$device/.config/kahlo/config"
	cat "/home/$device/private/isidore_files/$device/kahlo-channels" > "/home/$device/.config/kahlo/channels"
checkmark

log 'SETTING AUDIO PROFILES'
	# Start pipewire sockets manually, as they only get enabled (not started) on install
	systemctl --user start 'pipewire.socket' 'pipewire-pulse.socket'

	# Wait for pipewire to initialize
	sleep 10

	# Set profiles for each card
	for card in $(pactl list short cards | cut --fields 2); do
		if [[ "$card" =~ .*ESI_U24XL.* ]]; then
			pactl set-card-profile "$card" 'output:analog-stereo+input:analog-stereo'
		else
			pactl set-card-profile "$card" 'off'
		fi
	done
checkmark

log 'ADDING TODO NOTES'
	cat << EOF >> "/home/$device/TODO.md"

## kahlo
- copy cache
- copy unwatched videos
EOF
checkmark
