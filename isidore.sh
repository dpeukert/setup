#!/bin/bash

# Set shell options
set -e -u -o 'pipefail'
shopt -s 'nullglob'

# Get the root directory of our script
script_path="$(readlink -f "$0")"
dir="$(dirname "${script_path}")"

# Load our helper functions
# shellcheck source=util.sh
. "${dir}/util.sh"

# Get the device name from the user
devices="$(find "${dir}" -maxdepth 2 -path '*/config.env')"
devices="$(sed -nr 's|(.*/)*([^_].+)/config.env|\2|p' <<< "${devices}")"
devices="$(sort <<< "${devices}")"
mapfile -t devices <<< "${devices}"
inputselect 'Select the device you want to set up' "${devices[@]}"
device="${VALUE}"

# Load the device and platform configs and check that they have the required fields
loadconfig 'device' "${dir}/${device}/config.env"

if [[ -z "${device_platform:-}" ]]; then
	abort 1 "Platform for device ${device} not specified, aborting..."
fi

loadconfig 'platform' "${dir}/_platforms/${device_platform}/config.env"

if [[ ! -f "${dir}/_platforms/${device_platform}/platform" ]]; then
	abort 1 "Platform script for platform ${device_platform} not found, aborting..."
fi

chmod +x "${dir}/_platforms/${device_platform}/platform"

# Check if the platform uses tags
platform_uses_tags="${platform_uses_tags:-}"
# shellcheck disable=SC2310 # We act based on the return value of is_true/is_false
if is_false "${platform_uses_tags}"; then
	# This platform doesn't use tags, check if we got some anyway
	if [[ -n "${device_tags:-}" ]]; then
		abort 1 "Platform ${device_platform} does not support tags, aborting..."
	fi

	# We did not get tags (as expected), run the platform script
	"${dir}/_platforms/${device_platform}/platform" "${dir}" "${device}" 'install'

	# Exit afterwards, because we don't want to continue execution
	builtin exit 0
elif is_true "${platform_uses_tags}"; then
	# This platform does use tags, check if have a base tag, if not, abort
	if [[ -z "${platform_base_tag:-}" ]]; then
		abort 1 "Platform ${device_platform} has no base tag specified, aborting..."
	fi
else
	abort 1 "Platform ${device_platform} has an invalid uses_tags field, aborting..."
fi

# Gather the tags we want to use
if [[ -z "${device_tags:-}" ]]; then
	device_tags="${platform_base_tag}"
else
	device_tags="${platform_base_tag},${device_tags}"
fi

# Go through each tag, validate it and load its config
tag_list=''
tag_count="$(( "$(printf '%s' "${device_tags}" | grep -o ',' | wc -l)" + 1 ))"
tag_index=1

while [[ "${tag_index}" -le "${tag_count}" ]]; do
	tag="$(cut -d ',' -f "${tag_index}" <<< "${device_tags}")"
	tag="$(trimstring "${tag}")"

	# Validate the tag name
	if [[ -z "${tag}" ]]; then
		abort 1 "Device ${device} has an invalid tag, aborting..."
	fi

	# Clear tag weight in case it's not specified for the tag being processed
	tag_weight=''

	# Load the tag config and check that it has the required fields
	loadconfig 'tag' "${dir}/_tags/${tag}/config.env"

	if [[ "${tag_platform:-}" != "${device_platform}" ]]; then
		abort 1 "Platform ${tag_platform:-} for tag ${tag} does not match device platform ${device_platform}, aborting..."
	fi

	if [[ -z "${tag_weight:-}" ]]; then
		abort 1 "Weight for tag ${tag} not specified, aborting..."
	fi

	tag_list="${tag_list}${tag_weight};${tag}${newline}"
	tag_index="$(( "${tag_index}" + 1))"
done

# Sort the tag list and abort if there are any duplicates
tag_list_sorted="$(printf '%s' "${tag_list}" | sort -n -t ';' -k 1)"
duplicates="$(printf '%s' "${tag_list_sorted}" | cut -d ';' -f 1 | uniq -c | awk '{if($1 > 1) printf " and "$2}' | cut -c 6-)"

if [[ -n "${duplicates}" ]]; then
	abort 1 "Multiple tags have weights ${duplicates}, aborting..."
fi

# Prepare the final tag array
tags="$(echo "${tag_list_sorted}" | cut -d ';' -f 2)"
mapfile -t tags <<< "${tags}"

# Run the platform script with the tags as arguments
"${dir}/_platforms/${device_platform}/platform" "${dir}" "${device}" 'install' "${tags[@]}"
