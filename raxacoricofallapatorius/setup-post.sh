#!/bin/bash

log 'SETTING AUDIO PROFILES'
	# Start pipewire sockets manually, as they only get enabled (not started) on install
	systemctl --user start 'pipewire.socket' 'pipewire-pulse.socket'

	# Wait for pipewire to initialize
	sleep 10

	# Set profiles for each card
	for card in $(pactl list short cards | cut --fields 2); do
		if [[ "$card" =~ .*skl_hda_dsp_generic.* ]]; then
			pactl set-card-profile "$card" 'HiFi (HDMI1, HDMI2, HDMI3, Mic1, Mic2, Speaker)'
		else
			pactl set-card-profile "$card" 'off'
		fi
	done
checkmark
