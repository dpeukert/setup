#!/bin/bash

# User options
keymap='cz-qwertz'
font='eurlatgr'
country='CZ'
private_uuid='bbf066b8-d2c2-4b2e-a670-0a13bc029efe'

# Constants
ping_ip='1.1.1.1'
arch_root='/mnt'

# Helpers
startsudo () {
	echo "$device ALL=(ALL) NOPASSWD: ALL" | sudo tee '/etc/sudoers.d/99-temporary-nopasswd' > '/dev/null'
	trap stopsudo SIGINT SIGTERM
}

stopsudo () {
	sudo rm -f '/etc/sudoers.d/99-temporary-nopasswd'
	trap - SIGINT SIGTERM
}

runinchroot() {
	arch-chroot '/mnt' /bin/sh -c "$@"
}

updatemakepkgconf () {
	if [ "$#" -ne 0 ] && [ "$#" -ne 1 ]; then
		abort 1 'updatemakepkgconf: zero or one parameters must be provided'
	fi

	sudo sed "s/^#MAKEFLAGS.*$/MAKEFLAGS=-j$(nproc)/" -i "${1:-}/etc/makepkg.conf"
	sudo sed -E "s/^(OPTIONS=\(.*) debug /\1 \!debug /" -i "${1:-}/etc/makepkg.conf"
	sudo sed "s/^PKGEXT.*$/PKGEXT='.pkg.tar'/" -i "${1:-}/etc/makepkg.conf"
}

installpackages () {
	if [ "$#" -ne 1 ]; then
		abort 1 'installpackages: exactly one parameter must be provided'
	fi

	# Clear AUR cache beforehand, as some AUR packages don't build when using a cache
	# of an already failed build
	trizen -S --clean --aur

	# As we might be dealing with unreliable connections, retry when the command fails,
	# as we are using --needed, only packages that haven't yet been successfully installed
	# are processed in each loop, until all packages are installed
	while ! sed '/^#/ d' "$1" | trizen --noconfirm --needed -S -; do
		echo 'Unable to install all packages, trying again in 10 seconds'
		sleep 10

		# Clear AUR cache, see above for reasoning
		trizen -S --clean --aur
	done
}

uninstallpackages () {
	if [ "$#" -ne 1 ]; then
		abort 1 'uninstallpackages: exactly one parameter must be provided'
	fi

	echo "$1" | sudo pacman --noconfirm -Rns -
	sudo paccache -rk2
	sudo paccache -ruk0
}

autodetectapplies () { # $1 - autodetect directory path
	# Load device config
	loadconfig 'device' "${dir}/${device}/config.env"

	if [[ -n "${device_force_disable_autodetects:-}" ]]; then
		device_force_disable_autodetects=",${device_force_disable_autodetects},"
	else
		device_force_disable_autodetects=''
	fi

	if [[ -n "${device_force_enable_autodetects:-}" ]]; then
		device_force_enable_autodetects=",${device_force_enable_autodetects},"
	else
		device_force_enable_autodetects=''
	fi

	# Check if the autodetect is forced to be enabled
	if printf '%s' "${device_force_enable_autodetects}" | grep -q ",$(basename "$1"),"; then
		return 0
	fi

	# Check if the autodetect is forced to be disabled
	if printf '%s' "${device_force_disable_autodetects}" | grep -q ",$(basename "$1"),"; then
		return 1
	fi

	# Check if the autodetect script detected its condition
	if bash "$1/autodetect.sh"; then
		return 0
	else
		return 1
	fi
}
