#!/bin/vbash
# shellcheck shell=bash

# Include VyOS specific functions and aliases
echo '--- LOADING VYOS FUNCTIONS AND ALIASES ---'
# shellcheck source=/dev/null
. '/opt/vyatta/etc/functions/script-template'

# Load our helper functions
# shellcheck source=util.sh
. "/config/user-data/util.sh"

# Prepare our environment variables
echo '--- PREPARING ENVIROMENT VARIABLES ---'
LAN_NETWORK_PREFIX="$(get_dhcp_server_prefix)"
LAN_NETWORK_MASK="$(get_dhcp_server_mask)"

# Prepare BIND config from template
echo '--- PREPARING BIND CONFIG ---'
write_bind_config '/config/user-data/bind-config/named.conf.in'

# Prepare internal zone from template
echo '--- PREPARING INTERNAL ZONE ---'
write_internal_zone_file '/config/user-data/bind-config/dpk.es.zone.in'

# Prepare external zone from template
echo '--- PREPARING EXTERNAL ZONE ---'
write_external_zone_file "/config/user-data/bind-config/dpk.es.zone.in"
