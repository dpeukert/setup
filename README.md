# Isidore

`isidore` is a set of scripts that facilitates the installation and setup of the following devices:

| Hostname                                            | Type                                                                                                                                                                                    | OS                |
| --------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------- |
| [akhaten](akhaten/)                                 | personal laptop                                                                                                                                                                         | Arch Linux        |
| [raxacoricofallapatorius](raxacoricofallapatorius/) | work laptop                                                                                                                                                                             | Arch Linux        |
| [skaro](skaro/)                                     | desktop                                                                                                                                                                                 | Arch Linux        |
| [gallifrey](gallifrey/)                             | local server                                                                                                                                                                            | Arch Linux        |
| [trenzalore](trenzalore/)                           | [Vultr](https://www.vultr.com/?ref=8080415-4F) server                                                                                                                                   | Arch Linux        |
| [darillium](darillium/)                             | [CWWK Intel N100 based mini PC](https://cwwk.net/products/new-12th-gen-firewall-mini-pc-intel-i3-n305-n100-soft-router-2x10g-sfp-2xi226-v-2-5g-nvme-2xhd-ddr5-minipc-nas-server) router | VyOS              |
| [karn](karn/)                                       | [MikroTik cAP ac](https://mikrotik.com/product/cap_ac) AP                                                                                                                               | Mikrotik RouterOS |
| [mars](mars/)                                       | [MikroTik CRS309-1G-8S+IN](https://mikrotik.com/product/crs309_1g_8s_in) switch                                                                                                         | Mikrotik SwOS     |

<sup>The Vultr link is a referral link that gives you 50 bucks to try out Vultr. If you decide to use it, thanks!</sup>

## Structure
More information coming soon, in the meantime, check out the scripts themselves.

## Arch Linux instructions
1. Boot the [Arch Linux ISO](https://www.archlinux.org/download/)
2. Run `sh <(curl 'https://isidore.dpk.es')` (compare with the [isidore.dpk.es nginx config](gallifrey/files-setup/root/srv/docker/nginx/sites/isidore.dpk.es) before executing) and follow the prompts

## Mikrotik RouterOS instructions
1. Run `./isidore.sh` and follow the prompts
2. Copy the contents of the `output` directory to the flash storage of or a removable storage connected to the Mikrotik device
3. Run `/system reset-configuration skip-backup=yes no-defaults=yes run-after-reset=/path/to/output/output.rsc` on the Mikrotik device

## Instructions for other operating systems
1. Run `./isidore.sh` and follow the prompts

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`) with the following attributions:
* The [Bash function for validating IP addresses](https://www.linuxjournal.com/content/validating-ip-address-bash-script) by [Mitch Frazier](https://www.linuxjournal.com/users/mitch-frazier), used with an attribution in this README
  * part of `util.sh`
* A [script for automatically toggling VRR based on Sway window fullscreen status](https://gist.github.com/GrabbenD/adc5a7a863cbd1553461376cf4c50467) by [GrabbenD](https://github.com/GrabbenD), used with an attribution in this README and modified
  * `_tags/archlinux-pc/files-setup/root/usr/local/bin/vrr-auto-toggle`
