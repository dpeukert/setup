#!/bin/bash

log 'COPYING WORK FILES'
	# SSH
	cat "/home/$device/private/isidore_files/archlinux-work/ssh/config" >> "/home/$device/.ssh/config"

	cat "/home/$device/private/isidore_files/archlinux-work/ssh/id_rsa" > "/home/$device/.ssh/id_rsa"
	chmod 644 "/home/$device/.ssh/id_rsa.pub"
	chmod 600 "/home/$device/.ssh/id_rsa"

	# OpenVPN profile
	cat "/home/$device/private/isidore_files/archlinux-work/openvpn.conf" | sudo tee '/etc/openvpn/client/work.conf' > '/dev/null'
	sudo chmod 600 '/etc/openvpn/client/work.conf'

	# Beekeeper Studio
	mkdir -p "/home/$device/.config/beekeeper-studio/"
	cat "/home/$device/private/isidore_files/archlinux-work/beekeeper-studio/app.db" > "/home/$device/.config/beekeeper-studio/app.db"
	cat "/home/$device/private/isidore_files/archlinux-work/beekeeper-studio/.key" > "/home/$device/.config/beekeeper-studio/.key"
	chmod 600 "/home/$device/.config/beekeeper-studio/app.db" "/home/$device/.config/beekeeper-studio/.key"

	# kubectl config
	mkdir --parents "/home/$device/.kube/"
	cat "/home/$device/private/isidore_files/archlinux-work/kube-config" > "/home/$device/.kube/config"
	chmod 600 "/home/$device/.kube/config"

	# S3
	cp -r --no-preserve=mode "/home/$device/private/isidore_files/archlinux-work/s3/" "/home/$device/.s3cfg/"
	chmod 700 "/home/$device/.s3cfg/"
	chmod 600 "/home/$device/.s3cfg/"*

	# gcac
	cat "/home/$device/private/isidore_files/archlinux-work/gcac/api_keys" >> "/home/$device/.config/gcac/api_keys"
	cat "/home/$device/private/isidore_files/archlinux-work/gcac/providers" >> "/home/$device/.config/gcac/providers"

	# .npmrc
	cat "/home/$device/private/isidore_files/archlinux-work/npmrc" > "/home/$device/.npmrc"
	chmod 600 "/home/$device/.npmrc"
checkmark

log 'ADDING TODO NOTES'
	cat << EOF >> "/home/$device/TODO.md"

## Login
- Postman
EOF
checkmark
