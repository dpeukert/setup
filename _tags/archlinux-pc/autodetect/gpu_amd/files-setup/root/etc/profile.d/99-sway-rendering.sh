#!/bin/sh

# Set sway rendering options if relevant
if [ -f '/etc/is_pc' ]; then
	export WLR_RENDERER='vulkan'
fi
