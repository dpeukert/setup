server {
	include /etc/nginx/listen.conf;
	server_name isidore.dpk.es;

	include /etc/nginx/no-ai.conf;

	location = / {
		default_type 'text/plain; charset=utf-8';
		return 200 '#!/bin/sh

print_checkmark() {
	printf \' \\033[0;32m✓\\033[0m\\n\'
}

printf \'CREATING TEMPORARY DIRECTORY\'
dir="$dollar(mktemp --directory)"
print_checkmark

printf \'DOWNLOADING ISIDORE REPO\'
curl --location --silent --fail "https://gitlab.com/dpeukert/isidore/-/archive/main/isidore-main.tar.gz" | tar -xzC "$dollar{dir}" --strip-components 1 -f -
print_checkmark

bash "$dollar{dir}/isidore.sh"
';
	}

	location / {
		return 404;
	}

	include /etc/nginx/securitytxt.conf;
	include /etc/nginx/robotstxt-disallow.conf;
}
